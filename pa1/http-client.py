# Micah Switzer
# Networks PA1

from socket import socket, AF_INET, SOCK_STREAM
from sys import argv

# create the socket
clientSocket = socket(AF_INET, SOCK_STREAM)
# connect to the server
clientSocket.connect((argv[1], int(argv[2])))

# build the request string
requestContents = "GET " + argv[3] + " HTTP/1.1\r\nHost: " + argv[1] + " \r\n\r\n"
print (requestContents)

# send the request
clientSocket.send(requestContents.encode())
# shut down the connection to indicate we're done sending data to the server
clientSocket.shutdown(1)

# the output string
final = b''
# iterate until we've received all the data
while True:
    # grab the first 4096 bytes
    data = clientSocket.recv(4096)
    # make sure we got some data
    if (len(data) == 0): break
    # append the data we received to the final var
    final += data

# close the socket
clientSocket.close()
# print the results
print(final.decode())